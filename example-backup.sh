#!/bin/bash

# An example backup script using backup-helper. To be run daily.
# Note: The filename must stay the same from one call to aqnother or earlier files not be replaced
destination=/data/backups
filename=my-backup.tar.gz 
filename_temp=working...${filename}

# First include the backup script
backup_helper=./backup-helper.sh

if [ ! -f $backup_helper ]; then
    echo "The backup-helper.sh file is required. Not found at ${backup_helper}. Download it at https://bitbucket.org/akademy/backup-script"
	exit 1
fi

. ${backup_helper}

now=$(date)

echo "Backing up at ${now} to ${destination}/${filename} ..."

# Create a file (in some way, here we are exporting from postgres
postgres sh -c 'pg_dumpall -U postgres' | gzip --best > ${destination}/${filename_temp}

# When the export process is complete, rename it.
mv ${destination}/${filename_temp} ${destination}/${filename}

# Now pass the folder and filename into the script.
# Note: Backup-helper requires the file to be in the backup folder before calling it
backup_rotate_store ${destination} ${filename}

now=$(date)
echo "... ${now} backup complete."