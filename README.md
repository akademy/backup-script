# Simple Backup Script

A simple unix-shell rotating backup script which only requires the ubiquitous date command. 

As the days of the week change so do the files, last Monday's file gets replaced the following Monday. Last September's file gets replaced the following September.

## Simple Instructions

Run a cronjob each day, e.g.

    31 3 * * * /location/backup.sh > /location/logs/backup.log 2>&1

and in that backup.sh file pass in a file with the same name, e.g. :
    
	DEST=/data/backups
    FILENAME=testfile.txt
	
	echo "My testfile" > ${DEST}/${FILENAME}
	
	backup_rotate_store ${DEST} ${FILENAME}
	
## Stores	
    
The backup_rotate_store will make copies and replace when needed. After one year you'll have one one file for each of the 12 months, one for each of the last 4 weeks (plus one for the last days of the end of a month outside four weeks) and one for each of the last 7 days.

Months:

  - January.testfile.txt
  - February.testfile.txt
  - ...
  - December.testfile.txt

Weeks:

 - 1st.testfile.txt
 - 8th.testfile.txt
 - ...
 - 28th.testfile.txt

Days:

 - Sunday.testfile.txt
 - Monday.testfile.txt
 - ...
 - Saturday.testfile.txt

The maximum number of files stored is 12 + 5 + 7 = 24.

## Suggested Use

  1. Clone this repo into a sub folder of your project. 
  1. Copy the example-backup.sh into your own project 
     1. Edit the export part as needed (i.e. export the data you need into a file)
     1. Edit the backup-script.sh include to point to the right directory. 
 














 